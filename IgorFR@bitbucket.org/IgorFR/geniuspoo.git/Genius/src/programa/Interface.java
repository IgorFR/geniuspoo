package programa;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JToggleButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class Interface extends JFrame {

	private JPanel contentPane;
	protected Object audioInputStream1;
	private JButton botaoVerde;
	private JButton botaoAmarelo;
	private JButton botaoAzul;
	private boolean ligado = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface frame = new Interface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	
	/**
	 * Create the frame.
	 */
	public Interface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 467, 489);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JButton botaoVermelho = new JButton("");
		ActionListener piscarVermelho = new ActionListener(){
			@Override
            public void actionPerformed(ActionEvent event){
				botaoVermelho.setIcon(new ImageIcon(Interface.class.getResource("/programa/Red_Button(luz).png")));
				Sons.Reproduzir("botao_red", false);
				
				new Thread(new Runnable() {
					
					 public void run() {
						
						 try{
							 Thread.sleep(500);
							 botaoVermelho.setIcon(new ImageIcon(Interface.class.getResource("/programa/Red_Button.png")));
						 } 
						 catch (Exception e){
							e.printStackTrace();
						 }	
					 }
				}).start();
			}
		};
		botaoVermelho.setEnabled(false);
		botaoVermelho.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				botaoVermelho.setIcon(new ImageIcon(Interface.class.getResource("/programa/Red_Button(apertado).png")));
				Sons.Reproduzir("Pressionar", false);
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				botaoVermelho.setIcon(new ImageIcon(Interface.class.getResource("/programa/Red_Button.png")));

				Timer timer = new Timer(600, piscarVermelho);
				timer.setRepeats(false);
				timer.start();
			}
		});
		botaoVermelho.setForeground(Color.GREEN);
		botaoVermelho.setIcon(new ImageIcon(Interface.class.getResource("/programa/Red_Button.png")));
		botaoVermelho.setBounds(229, 12, 211, 208);
		botaoVermelho.setVisible(false);
		
		JToggleButton tglbtnNewToggleButton = new JToggleButton("");
		tglbtnNewToggleButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				tglbtnNewToggleButton.setIcon(new ImageIcon(Interface.class.getResource("/programa/Ligar(apertado).png")));
				Sons.Reproduzir("Pressionar", false);
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				tglbtnNewToggleButton.setIcon(new ImageIcon(Interface.class.getResource("/programa/Ligar.png")));
				
				if(ligado == false){
					botaoVermelho.setVisible(true);
					botaoVermelho.setEnabled(true);
					botaoAmarelo.setVisible(true);
					botaoAmarelo.setEnabled(true);
					botaoAzul.setVisible(true);
					botaoAzul.setEnabled(true);
					botaoVerde.setVisible(true);
					botaoVerde.setEnabled(true);
					
					ligado = true;
				}
				else{
					botaoVermelho.setVisible(false);
					botaoVermelho.setEnabled(false);
					botaoAmarelo.setVisible(false);
					botaoAmarelo.setEnabled(false);
					botaoAzul.setVisible(false);
					botaoAzul.setEnabled(false);
					botaoVerde.setVisible(false);
					botaoVerde.setEnabled(false);
					
					ligado = false;
					System.exit(0);
				}
			}
		});
		tglbtnNewToggleButton.setIcon(new ImageIcon(Interface.class.getResource("/programa/Ligar.png")));
		tglbtnNewToggleButton.setBounds(240, 248, 24, 25);
		tglbtnNewToggleButton.setBorder(null);
		tglbtnNewToggleButton.setContentAreaFilled(false);
		tglbtnNewToggleButton.setFocusPainted(false);
		contentPane.add(tglbtnNewToggleButton);
		botaoVermelho.setBorder(null);
		botaoVermelho.setContentAreaFilled(false);
		botaoVermelho.setFocusPainted(false);
		contentPane.add(botaoVermelho);
		
		botaoAmarelo = new JButton("");
		ActionListener piscarAmarelo = new ActionListener(){
			@Override
            public void actionPerformed(ActionEvent event){
				botaoAmarelo.setIcon(new ImageIcon(Interface.class.getResource("/programa/Yellow_Button(luz).png")));
				Sons.Reproduzir("botao_yellow", false);
				
				new Thread(new Runnable() {
					
					 public void run() {
						
						 try{
							 Thread.sleep(500);
							 botaoAmarelo.setIcon(new ImageIcon(Interface.class.getResource("/programa/Yellow_Button.png")));
						 } 
						 catch (Exception e){
							e.printStackTrace();
						 }	
					 }
				}).start();
			}
		};
		botaoAmarelo.setEnabled(false);
		botaoAmarelo.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				botaoAmarelo.setIcon(new ImageIcon(Interface.class.getResource("/programa/Yellow_Button(apertado).png")));
				Sons.Reproduzir("Pressionar", false);
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				botaoAmarelo.setIcon(new ImageIcon(Interface.class.getResource("/programa/Yellow_Button.png")));

				Timer timer = new Timer(600, piscarAmarelo);
				timer.setRepeats(false);
				timer.start();
			}
		});
		
		botaoAmarelo.setForeground(Color.GREEN);
		botaoAmarelo.setIcon(new ImageIcon(Interface.class.getResource("/programa/Yellow_Button.png")));
		botaoAmarelo.setBounds(14, 232, 204, 203);
		botaoAmarelo.setVisible(false);
		botaoAmarelo.setBorder(null);
		botaoAmarelo.setContentAreaFilled(false);
		botaoAmarelo.setFocusPainted(false);
		contentPane.add(botaoAmarelo);
		
		botaoAzul = new JButton("");
		ActionListener piscarAzul = new ActionListener(){
			@Override
            public void actionPerformed(ActionEvent event){
				botaoAzul.setIcon(new ImageIcon(Interface.class.getResource("/programa/Blue_Button(luz).png")));
				Sons.Reproduzir("botao_blue", false);
				
				new Thread(new Runnable() {
					
					 public void run() {
						
						 try{
							 Thread.sleep(500);
							 botaoAzul.setIcon(new ImageIcon(Interface.class.getResource("/programa/Blue_Button.png")));
						 } 
						 catch (Exception e){
							e.printStackTrace();
						 }	
					 }
				}).start();
			}
		};
		botaoAzul.setEnabled(false);
		botaoAzul.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				botaoAzul.setIcon(new ImageIcon(Interface.class.getResource("/programa/Blue_Button(apertado).png")));
				Sons.Reproduzir("Pressionar", false);
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				botaoAzul.setIcon(new ImageIcon(Interface.class.getResource("/programa/Blue_Button.png")));
				
				Timer timer = new Timer(600, piscarAzul);
				timer.setRepeats(false);
				timer.start();
			}
		});
		botaoAzul.setForeground(Color.GREEN);
		botaoAzul.setIcon(new ImageIcon(Interface.class.getResource("/programa/Blue_Button.png")));
		botaoAzul.setBounds(231, 231, 204, 203);
		botaoAzul.setVisible(false);
		botaoAzul.setBorder(null);
		botaoAzul.setContentAreaFilled(false);
		botaoAzul.setFocusPainted(false);
		contentPane.add(botaoAzul);
		
		botaoVerde = new JButton("");
		ActionListener piscarVerde = new ActionListener(){
			@Override
            public void actionPerformed(ActionEvent event){
				botaoVerde.setIcon(new ImageIcon(Interface.class.getResource("/programa/Green_Button(luz).png")));
				Sons.Reproduzir("botao_green", false);
				
				new Thread(new Runnable() {
					
					 public void run() {
						
						 try{
							 Thread.sleep(500);
							 botaoVerde.setIcon(new ImageIcon(Interface.class.getResource("/programa/Green_Button.png")));
						 } 
						 catch (Exception e){
							e.printStackTrace();
						 }	
					 }
				}).start();
			}
		};
		
		botaoVerde.setEnabled(false);
		botaoVerde.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				botaoVerde.setIcon(new ImageIcon(Interface.class.getResource("/programa/Green_Button(apertado).png")));
				Sons.Reproduzir("Pressionar", false);
				
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				botaoVerde.setIcon(new ImageIcon(Interface.class.getResource("/programa/Green_Button.png")));
				
				Timer timer = new Timer(600, piscarVerde);
				timer.setRepeats(false);
				timer.start();
			}
		});
		botaoVerde.setForeground(Color.GREEN);
		botaoVerde.setIcon(new ImageIcon(Interface.class.getResource("/programa/Green_Button.png")));
		botaoVerde.setBounds(16, 15, 204, 203);
		botaoVerde.setVisible(false);
		botaoVerde.setBorder(null);
		botaoVerde.setContentAreaFilled(false);
		botaoVerde.setFocusPainted(false);
		contentPane.add(botaoVerde);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Interface.class.getResource("/programa/Background.jpg")));
		label.setBounds(0, 0, 451, 450);
		contentPane.add(label);
	}
}
